package WON;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.EmptyClipboardException;
import com.sk89q.worldedit.FilenameException;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;

public class WON extends JavaPlugin implements Listener{

	public String name = "World";
	public World world = Bukkit.getWorld(name);
	public World world2 = Bukkit.getWorld(name);
	public int x;
	public int y;
	public int z;
	public WorldEditPlugin api;
	public void onEnable(){
		    
		
			api = (WorldEditPlugin) this.getServer().getPluginManager().getPlugin("WorldEdit");
		
			System.out.println("True");
			
			this.getServer().createWorld(WorldCreator.name(name));
			Random random = new Random();
	        Bukkit.getPluginManager().registerEvents(this, this);
	        Bukkit.getPluginManager().registerEvents(new Interact(), this);
	        int x = random.nextInt(100) + 1;
	        int y = 200;
	        int z = random.nextInt(100) + 1;
	        
			this.x = x;
			this.y = y;
			this.z = z;
			Location l = new Location(world ,x, y, z);
			Bukkit.getWorld(name).setSpawnLocation(x, y, z);
			
			System.out.println("X=" + x + ",Y=" + y + ",Z=" +z);
			//Bukkit.getPluginManager().registerEvents(new WON(), this);
		
			
			if (api == null) {
			  // then don't try to use TerrainManager!
			}
			 
			// create a terrain manager object
			// OR - without needing an associated Player
			TerrainManager tm = new TerrainManager(api, world);
			 
			// don't include an extension - TerrainManager will auto-add ".schematic"
			File saveFile = new File(this.getDataFolder(), "Spawn");
			 
			// save the terrain to a schematic file
			
			 
			// reload a schematic
			
			//try {
			  // reload at the given location
			  
			  //tm.loadSchematic(saveFile, l);
			  // OR
			  // reload at the same place it was saved
			  //tm.loadSchematic(saveFile);
			//} catch (FilenameException e) {
			  //e.printStackTrace();
			//} catch (DataException e) {
			  //e.printStackTrace();
			//} catch (IOException e) {
			  //e.printStackTrace();
			//} catch (MaxChangedBlocksException e) {
			  //e.printStackTrace();
			//} catch (EmptyClipboardException e) {
			  //e.printStackTrace();
			//}
	}
	public void onDisable(){
		
		Bukkit.getServer().getWorld(name).getWorldFolder().deleteOnExit();
		
	}
	
	
	
	
	
 
	@EventHandler
	public void onjoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		
		Location loc = new Location(world, x , y, z);
		p.teleport(loc);
		e.setJoinMessage(null);
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if (e.getEntityType() == EntityType.PLAYER){
			Player p = e.getEntity();
			p.setHealth(20);
			
			p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 1, 20000000));
			p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 1, 100000));
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 1, 100000000));
			p.setHealth(20);
			Location loc = new Location(world, x , y, z);
			p.teleport(loc);
		}
		
	}
   
  
}
