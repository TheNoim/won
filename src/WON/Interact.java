package WON;

import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.sk89q.worldedit.blocks.ItemType;

public class Interact implements Listener{

	@EventHandler
	public void onInter(PlayerInteractEvent e){
		
		if (e.getAction() == Action.RIGHT_CLICK_AIR){
			//if (e.hasBlock()){
				FallingBlock fb = e.getPlayer().getLocation().getWorld().spawnFallingBlock(e.getPlayer().getLocation(),e.getMaterial(), (byte) 0); 
				Vector v = e.getPlayer().getLocation().getDirection().multiply(1.5).setY(2).multiply(0.7);
				fb.setVelocity(v);
				
				if (e.getItem().getAmount() > 1){
					e.getItem().setAmount(e.getItem().getAmount() - 1);
				}
				if (e.getItem().getAmount() == 1){
					e.getItem().setType(Material.AIR);
					e.getPlayer().getItemInHand().setType(Material.AIR);
				}
			//}
		}
		
		
	}
	
}
